#include <iostream>
#include <sstream>
#include <algorithm>
#include <fstream>
#include "Book.h"
#include "hash.h"
#include "MyVector.h"

/*
 main.cpp
 M00735091
 Created: 3/4/2021
 Updated:12/4/2021
 */

// Creating global variables.
std::string filename = "books";
LinearProbingHash<std::string, Book> books;

#define RED "\033[31m"  /* Red */
#define RESET "\033[0m" /* RESET */

void ReadFile();
void mainmenu();
void SearchData();
void UserData();
void RemoveData();
void LoadFile();
void WriteFile();
std::string lowercase(std::string str);

/*
  Display the Menu options and Move to required functions accordingly
*/
void mainmenu()
{
  int choice;
  system("clear");
  std::cout << "\n\n\n\t\t|========= WELCOME TO LIBRARY MANAGEMENT =========|";
  std::cout << "\n\n\n\t\t\t (1) Search Book " << std::endl;
  std::cout << "\t\t\t (2) Add Books " << std::endl;
  std::cout << "\t\t\t (3) Remove Books " << std::endl;
  std::cout << "\t\t\t (4) Load a Different File " << std::endl;
  std::cout << "\t\t\t (0) Exit " << std::endl;
  std::cout << "\n\n\t\t\t Enter a Choice: ";
  std::cin >> choice;

  switch (choice)
  {
  case 1:
    SearchData();
    break;
  case 2:
    UserData();
    break;
  case 3:
    RemoveData();
    break;
  case 4:
    LoadFile();
    break;
  case 0:
    WriteFile();
    std::cout << "\n\n\t\t ThankYou For Using Library Management." << std::endl;
    exit(0);
    break;
  default:
    std::cout << RED << "\n\n\n\t\t   Wrong Choice Entered! Try Again" << RESET;
    mainmenu();
    break;
  }
}

/*
  Read the file details into a HashMap 
*/
void ReadFile()
{
  std::ifstream Read(filename); // ifstream to read from the file

  if (!Read) // checks if the data exists in the file
  {
    int choice;
    std::cout << RED << "\n\n\t\t File Empty " << RESET;
    std::cout << "\n\n\t\t (1) Add Books ";
    std::cout << "\n\t\t (2) Change File ";
    std::cout << "\n\t\t Enter a Choice: ";
    std::cin >> choice;
    if (choice == 1)
      UserData();
    else if (choice == 2)
      LoadFile();
    else
    {
      std::cout << RED << "\n\n\n\t\t\t Wrong Choice Entered! Try Again" << RESET;
      //close fstream
      Read.close();
      ReadFile();
    }
    std::cout << "\n\n\t\t Press Enter Key To Continue.";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  else
  {
    std::string line, word, title, author, ISBN;
    int qty;
    vec<std::string> row;

    // Traverse the file
    while (!Read.eof())
    {
      row.clear();         //Clears row variable
      getline(Read, line); //stores the line read into variable 'line'
      if (line == "")      // checks if its empty line and skips
        continue;
      //breaks the line into words
      std::stringstream s(line);
      while (getline(s, word, '\t')) // loop to remove the '\t' and save to vector row
        row.push_back(word);

      title = (row[0]);   // assigns the first element to Title
      author = (row[1]);  // assigns the second element to Author
      ISBN = (row[2]);    // assigns the third element to ISBN
      qty = stoi(row[3]); // assigns the fourth element to Quantity

      //function to write book details.
      books.insert(lowercase(title), Book(title, author, ISBN, qty));
    }
    //close fstream
    Read.close();
  }
}

/*
  Function to ask user Title of book and display details if found.
*/
void SearchData()
{
  std::string title;
  char ch;
  do // loop to ask user title to search
  {
    ch = 'y';
    std::cout << "\n\t\t Enter the Title to Search: ";
    std::cin.ignore();
    getline(std::cin, title);
    std::cout << "\n\t\t Title Entered: " << title << "\n\t\t Search this? (Y / N) ";
    std::cin >> ch;
  } while (ch == 'N' || ch == 'n');

  title = lowercase(title); //converts the title entered into all lowercase.

  if (books.contains(title)) //checks if the book of entered title exists.
  {
    std::cout << "\n\t\t The Entered Title has following Details" << std::endl;
    books[title].List(); //function to displays book details.
  }
  else
    std::cout << "\n\t\t The Entered Title has No Details Found! " << std::endl;

  std::cout << "\n\n\t\t Enter to go back to Main Menu:";
  std::cin.get();
  std::cin.ignore(10, '\n');
  mainmenu();
}

/*
  Asks user book related details and write data to Data structure.
*/
void UserData()
{
  //variables to save item details
  std::string title, Title, author, temp;
  int qty;
  long ISBN = 0;
  char ch;
  system("clear");

  std::cout << "\n\n\n\t\t\t Enter Title: ";
  std::cin.ignore();
  getline(std::cin, title);

  Title = lowercase(title);  // converts title lowercase
  if (books.contains(Title)) // checks if book with the title entered exists
  {
    std::cout << "\n\t\t\t Book Already Present, Updating Quantity!";
    do // loop to enter book quantity
    {
      qty = 0;
      std::cout << "\n\n\t\t\t Enter Quantity (Should be greater than 0): ";
      std::cin >> qty;
    } while (qty <= 0);
    books[Title].setQty(qty); // updates quantity of book
  }
  else
  {
    do // loop to enter author details
    {
      ch = 't';
      std::cout << "\t\t\t Enter Author: ";
      std::cin.ignore();
      std::getline(std::cin, temp);
      std::cout << "\t\t\t Enter another Author(Y or N): ";
      std::cin >> ch;
      if (ch == 'y' || ch == 'Y')
        author += temp + " ;";
      else
        author += temp;
    } while (ch == 'y' || ch == 'Y');
    do //loop to enter ISBN details
    {
      std::cout << "\t\t\t Enter ISBN (Should be greater than 0): ";
      std::cin.ignore();
      std::cin >> ISBN;
    } while (ISBN <= 0);
    do // loop to enter quantity
    {
      qty = 0;
      std::cout << "\t\t\t Enter Quantity (Should be greater than 0): ";
      std::cin >> qty;
    } while (qty <= 0);

    //writes details to class using constructor declaration.
    Book b(title, author, std::to_string(ISBN), qty);
    //function to write book details to data structure
    books.insert(lowercase(title), b);
  }
  std::cout << "\n\t\t\t\t New Books Added!" << std::endl;
  std::cout << "\n\n\t\t\t   Enter to go back to Main Menu:";
  std::cin.get();
  std::cin.ignore(10, '\n');
  mainmenu();
}

/*
  Function to remove book if user entered title is found.
*/
void RemoveData()
{
  std::string title;
  char ch;
  do //loop to enter title to remove
  {
    ch = 'y';
    std::cout << "\n\t\t Enter the Title to Remove: ";
    std::cin.ignore();
    getline(std::cin, title);
    std::cout << "\n\t\t Title Entered: " << title << "\n\t\t Search this? (Y / N) ";
    std::cin >> ch;
  } while (ch == 'N' || ch == 'n');

  title = lowercase(title); // converts title lto lowercase

  if (books.contains(title)) // check if book entered title exists
  {
    std::cout << "\n\t\t The Entered Title has following Details" << std::endl;
    books[title].List(); // dispaly book details
    std::cout << "\n\t\t Removed Book from Stock!" << std::endl;
    if (books[title].getQty() - 1 == 0)
      books.remove(title); //if Quantity is less than 1, then remove book entirely from data structure.
    else
      books[title].cutQty(); //if Quantity is more than 1, then remove reduce it by 1
  }
  else
    std::cout << "\n\t\t The Entered Title has No Details Found! " << std::endl;

  std::cout << "\n\n\t\t Enter to go back to Main Menu:";
  std::cin.get();
  std::cin.ignore(10, '\n');
  mainmenu();
}

/*
  Function to change current file to another user entered file
*/
void LoadFile()
{
  std::string temp;
  char ch;
  do //loop to enter new filename
  {
    ch = 'y';
    std::cout << "\n\t\t Enter the File to change to: ";
    std::cin.ignore();
    getline(std::cin, temp);
    std::cout << "\n\t\t File Entered: " << temp << "\n\t\t Change to this? (Y / N) ";
    std::cin >> ch;
  } while (ch == 'N' || ch == 'n');
  WriteFile();     //writes previous data to file
  books.clear();   //clears the data structure
  filename = temp; // changes filename
  std::cout << "\n\t\t\t File Changed!";
  ReadFile(); //loads details from changed file
  std::cout << "\n\n\t\t     Enter to go back to Main Menu:";
  std::cin.get();
  std::cin.ignore(10, '\n');
  mainmenu();
}

/*
  Function to write all Book details to file 
*/
void WriteFile()
{
  std::string tempfile = "temp";
  std::fstream Write(tempfile, std::ios::out | std::ios::app);
  for (const std::string &title : books.keys()) //loop to read through
  {
    Write << books.get(title).to_tsv();
  }
  Write.close();
  remove(filename.c_str());                   // removing the existing file
  rename(tempfile.c_str(), filename.c_str()); // renaming the updated file with the existing file name
}

  /*
    method for returning a string in all lowercase
    @param str, string that needs to be lowercase 
    @return lowercase string
  */
std::string lowercase(std::string str)
{
  std::transform(str.begin(), str.end(), str.begin(),
                 [](unsigned char c) { return std::tolower(c); });
  return str;
}

int main()
{
  ReadFile();
  mainmenu();
}