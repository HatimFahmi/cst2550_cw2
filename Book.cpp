#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include "Book.h"
/*
 Book.cpp
 M00735091
 Created: 3/4/2021
 Updated: 11/4/2021
 */
Book::Book(){};
Book::Book(std::string title, std::string author, std::string ISBN, int qty)
{
  this->title = title;
  this->author = author;
  this->ISBN = ISBN;
  this->qty = qty;
}

std::string Book::getTitle()
{
  return title;
}
std::string Book::getISBN()
{
  return ISBN;
}
std::string Book::getAuthor()
{
  return author;
}
int Book::getQty()
{
  return qty;
}
void Book::setQty(int qty)
{
  this->qty += qty;
}
void Book::cutQty()
{
  --this->qty;
}
void Book::List()
{
  std::cout << "\n\t\t--------- Book Details ---------";
  std::cout << "\n\n\t\t Title: " << getTitle();
  std::cout << "\n\t\t Author: " << getAuthor();
  std::cout << "\n\t\t ISBN: " << getISBN();
  std::cout << "\n\t\t Quantity: " << getQty();
}
std::string Book::to_tsv()
{
  std::ostringstream buffer;
  buffer << getTitle() << '\t' << getAuthor() << '\t' << getISBN() << '\t' << getQty()
         << '\t' << std::endl;
  return buffer.str();
}