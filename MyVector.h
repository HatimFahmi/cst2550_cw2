#ifndef _VEC_H
#define _VEC_H

/*
 MyVector.h
 M00735091
 Created: 12/4/2021
 Updated:12/4/2021
 */

#include <cstring>
#include <functional>
#include <iostream>
#include <optional>
#include <utility>

/**
 * @brief Resizable array implementation. Minimal API, but implemented methods
 * are compliant with std::vector (so behaves as a drop-in replacement if using
 * a subset of methods).
 *
 * Also obeys the "Rule of Five" so you can feel free to pass around `vec`
 * instances without worrying about broken copy-constructors.
 *
 * @tparam T The type of elements you would like to store.
 */
template <typename T>
class vec
{
protected:
    T *internal_buffer;
    size_t length;
    size_t capacity;
    void grow();

public:
    vec();
    vec(std::initializer_list<T> init_list);
    ~vec();

    T *data();
    const T *begin() const;
    const T *end() const;

    bool empty();
    size_t size() const;
    void reserve(size_t space);

    void push_back(const T &value);
    void push_front(const T &value);
    T pop_back();
    T pop_front();

    std::optional<size_t> find(const T &search_value);
    void remove(int index);
    void clear();

    /* Rule of Five */
    vec(const vec &other);                // copy constructor
    vec(vec &&other) noexcept;            // move constructor
    vec &operator=(const vec &other);     // copy assignment
    vec &operator=(vec &&other) noexcept; // move assignment

    /* Operators */
    T &operator[](int index) const;
};

template <typename T>
vec<T>::vec() : internal_buffer(nullptr)
{
    clear();
}

template <typename T>
vec<T>::vec(std::initializer_list<T> init_list) : internal_buffer(nullptr)
{
    const T *init_list_ptr = init_list.begin();

    length = init_list.size();
    capacity = length;
    internal_buffer = new T[capacity];

    for (size_t i = 0; i < length; i++)
    {
        internal_buffer[i] = init_list_ptr[i];
    }
}

template <typename T>
vec<T>::~vec()
{
    delete[] internal_buffer;
}

template <typename T>
size_t vec<T>::size() const
{
    return length;
}

template <typename T>
bool vec<T>::empty()
{
    return size() == 0;
}

template <typename T>
T *vec<T>::data()
{
    return internal_buffer;
}

template <typename T>
const T *vec<T>::begin() const
{
    return internal_buffer;
}

template <typename T>
const T *vec<T>::end() const
{
    return internal_buffer + size();
}

template <typename T>
void vec<T>::grow()
{
    capacity = capacity * 2;

    T *new_buffer = new T[capacity];
    for (size_t i = 0; i < length; i++)
    {
        new_buffer[i] = internal_buffer[i];
    }

    delete[] internal_buffer;
    internal_buffer = new_buffer;
}

template <typename T>
void vec<T>::push_back(const T &value)
{
    // not enough space
    if (capacity < length + 1)
        grow();

    internal_buffer[length++] = value;
}

template <typename T>
void vec<T>::push_front(const T &value)
{
    // not enough space
    if (capacity < length + 1)
        grow();

    // move all elements one step forward, starting from 0 to free up index 0
    for (size_t i = 0; i < length; i++)
    {
        internal_buffer[i + 1] = internal_buffer[i];
    }

    internal_buffer[0] = value;
}

template <typename T>
T vec<T>::pop_back()
{
    return internal_buffer[--length];
}

template <typename T>
std::optional<size_t> vec<T>::find(const T &search_value)
{
    /*
        naive O(n) search as we cannot make any assumptions
        about T & whether the `vec` is sorted or not.
    */
    for (size_t i = 0; i < length; i++)
    {
        if (internal_buffer[i] == search_value)
        {
            return i;
        }
    }

    return std::nullopt;
}

template <typename T>
void vec<T>::remove(int index)
{
    if (index < 0 || (size_t)index >= length)
    {
        // no-op if index is invalid
        return;
    }

    length--;

    // nothing more to be done
    if (index == length)
        return;

    // move all elements one index back, starting from the provided index
    // effectively erasing the element at pprovided index
    for (size_t i = index; i < length; i++)
    {
        internal_buffer[i] = internal_buffer[i + 1];
    }
}

template <typename T>
void vec<T>::reserve(size_t space)
{
    while (capacity < space)
    {
        grow();
    }

    for (size_t i = 0; i < space; i++)
    {
        internal_buffer[i] = T();
    }
}

template <typename T>
void vec<T>::clear()
{
    length = 0;
    capacity = 8;
    delete[] internal_buffer;
    internal_buffer = new T[capacity];
}

template <typename T>
vec<T>::vec(const vec<T> &other) : internal_buffer(nullptr)
{
    const T *other_ptr = other.begin();

    length = other.length;
    capacity = other.capacity;

    delete[] internal_buffer;
    internal_buffer = new T[capacity];

    for (size_t i = 0; i < length; i++)
    {
        internal_buffer[i] = other_ptr[i];
    }
}

template <typename T>
vec<T>::vec(vec<T> &&other) noexcept
    : internal_buffer(std::exchange(other.internal_buffer, nullptr)),
      length(other.length),
      capacity(other.capacity) {}

template <typename T>
vec<T> &vec<T>::operator=(const vec<T> &other)
{
    return *this = vec<T>(other);
}

template <typename T>
vec<T> &vec<T>::operator=(vec<T> &&other) noexcept
{
    std::swap(internal_buffer, other.internal_buffer);
    std::swap(capacity, other.capacity);
    std::swap(length, other.length);
    return *this;
}

template <typename T>
T &vec<T>::operator[](int index) const
{
    return internal_buffer[index];
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const vec<T> &list)
{
    os << '[';

    size_t length = list.size();
    for (size_t i = 0; i < length; i++)
    {
        os << list[i];
        if (i != length - 1)
            os << ", ";
    }

    os << ']';

    return os;
}

#endif
