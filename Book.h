#ifndef _BOOK_H_
#define _BOOK_H_
#include<iostream>

/*
 Book.h
 M00735091
 Created: 3/4/2021
 Updated: 11/4/2021
*/

class Book
{
private:
  std::string author;
  std::string ISBN;
  std::string title;
  int qty;

public:
  Book();
  Book(std::string title, std::string author, std::string ISBN, int qty);

  std::string getISBN();
  int getQty();
  void setQty(int);
  void cutQty();
  std::string getAuthor();
  std::string getTitle();

  /*
    method for displaying book details
  */
  void List();
/*
    method for returning book details in string buffer form
    @return variable in vector
  */
  std::string to_tsv();

};
#endif