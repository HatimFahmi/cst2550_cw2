#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "catch.hpp"
#include "../Book.h"
#include "../hash.h"

/*
 test_case.cpp
 M00735091
 Created: 11/4/2021
 Updated: 14/4/2021
 */

LinearProbingHash<std::string, Book> books;
std::streambuf *original_cin = std::cin.rdbuf();
std::streambuf *original_cout = std::cout.rdbuf();
std::istringstream test_input;
std::ostringstream test_output;

std::string lowercase(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return str;
}

void RemoveData(std::string title)
{
    title = lowercase(title);

    if (books.contains(title))
    {
        if (books[title].getQty() - 1 == 0)
            books.remove(title);
        else
            books[title].cutQty();
    }
    else
        std::cout << "\n\t\t The Entered Title has No Details Found! " << std::endl;
}

void UserData()
{
    //variables to save item details
    std::string title, Title, author, temp;
    int qty;
    long ISBN = 0;
    char ch;
    system("clear");

    std::cout << "\n\n\n\t\t\t Enter Title: ";
    std::cin.ignore();
    getline(std::cin, title);

    Title = lowercase(title);  // converts title lowercase
    if (books.contains(Title)) // checks if book with the title entered exists
    {
        std::cout << "\n\t\t\t Book Already Present, Updating Quantity!";
        do // loop to enter book quantity
        {
            qty = 0;
            std::cout << "\n\n\t\t\t Enter Quantity (Should be greater than 0): ";
            std::cin >> qty;
        } while (qty <= 0);
        books[Title].setQty(qty); // updates quantity of book
    }
    else
    {
        do // loop to enter author details
        {
            ch = 't';
            std::cout << "\t\t\t Enter Author: ";
            std::cin.ignore();
            std::getline(std::cin, temp);
            std::cout << "\t\t\t Enter another Author(Y or N): ";
            std::cin >> ch;
            if (ch == 'y' || ch == 'Y')
                author += temp + " ;";
            else
                author += temp;
        } while (ch == 'y' || ch == 'Y');
        do //loop to enter ISBN details
        {
            std::cout << "\t\t\t Enter ISBN (Should be greater than 0): ";
            std::cin.ignore();
            std::cin >> ISBN;
        } while (ISBN <= 0);
        do // loop to enter quantity
        {
            qty = 0;
            std::cout << "\t\t\t Enter Quantity (Should be greater than 0): ";
            std::cin >> qty;
        } while (qty <= 0);

        //writes details to class using constructor declaration.
        Book b(title, author, std::to_string(ISBN), qty);
        //function to write book details to data structure
        books.insert(lowercase(title), b);
    }
}

bool SearchData()
{
    std::string title;
    char ch;
    do // loop to ask user title to search
    {
        ch = 'y';
        std::cout << "\n\t\t Enter the Title to Search: ";
        std::cin.ignore();
        getline(std::cin, title);
        std::cout << "\n\t\t Title Entered: " << title << "\n\t\t Search this? (Y / N) ";
        std::cin >> ch;
    } while (ch == 'N' || ch == 'n');

    title = lowercase(title); //converts the title entered into all lowercase.

    if (books.contains(title)) //checks if the book of entered title exists.
    {
        std::cout << "\n\t\t The Entered Title has following Details" << std::endl;
        books[title].List(); //function to displays book details.
        return true;
    }
    else
    {
        std::cout << "\n\t\t The Entered Title has No Details Found! " << std::endl;
        return false;
    }
}

TEST_CASE("Test for making string lowercase", "[lowercase()]")
{
    REQUIRE(lowercase("HARRY") == "harry");
    REQUIRE(lowercase("ABC") == "abc");
}

TEST_CASE("Test for Removing books", "[RemoveData()]")
{
    books.insert("harry", Book("harry", "ABC", "34570", 5));
    RemoveData("harry");
    REQUIRE(books.contains("harry") == true);
    REQUIRE(books["harry"].getQty() == 4);

    books.clear();
}

TEST_CASE("Test for Adding New Books From User", "[UserData()]")
{
    test_input.clear();
    test_input.str("\nharry\n ABC\nh\n34570\n5\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    UserData();

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    REQUIRE(books.contains("harry") == true);
    REQUIRE(books["harry"].getAuthor() == "ABC");
    REQUIRE(books["harry"].getISBN() == "34570");
    REQUIRE(books["harry"].getQty() == 5);

    test_input.clear();
    test_input.str("\nharry\n5\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    UserData();

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    REQUIRE(books["harry"].getQty() == 10);

    books.clear();
}

TEST_CASE("Test for searching a Book", "[SearchData()")
{
    books.insert("harry", Book("harry", "ABC", "34570", 5));

    test_input.clear();
    test_input.str(" harry\n y\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE(SearchData() == true);

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);
    books.clear();
}

TEST_CASE("Test for returning book details as output stream", "[Book.to_tsv()]")
{
    Book b("harry", "ABC", "34570", 5);
    std::ostringstream expected_output;
    expected_output << b.getTitle() << '\t' << b.getAuthor() << '\t' << b.getISBN()
                    << '\t' << b.getQty() << '\t' << std::endl;

    REQUIRE(b.to_tsv() == expected_output.str());
}

TEST_CASE("Test for displaying book details to the user", "[Book.List()]")
{
    Book b("harry", "ABC", "34570", 5);

    test_input.clear();
    test_input.str("");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());
    
    b.List();
    
    REQUIRE(test_output.str() == "\n\t\t--------- Book Details ---------\n\n\t\t Title: harry\n\t\t Author: ABC\n\t\t ISBN: 34570\n\t\t Quantity: 5");

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);
}